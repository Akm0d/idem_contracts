# This file ensures that the pre_present/post_present contracts exist
# It doesn't do anything with the values as execution order of these contracts isn't guaranteed

def pre_present(hub, ctx):
    print("pre_present")


def post_present(hub, ctx):
    # This doesn't get called because idem already has a post_present contract
    print("post_present")
    return ctx.ret
