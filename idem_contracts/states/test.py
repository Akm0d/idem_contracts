__contracts__ = ["resource"]


def present(hub, ctx, name: str, **kwargs):
    return {"result": True, "comment": [], "changes": {}, "name": name}


def absent(hub, ctx, name: str, **kwargs):
    return {}


async def describe(hub, ctx):
    return {}
