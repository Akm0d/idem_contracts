==============
idem_contracts
==============

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

Show the order of contracts and demonstrate how to do contracts on a contract

About
=====

Getting Started
===============

Prerequisites
-------------

* Python 3.8+
* git *(if installing from source, or contributing to the project)*

Installation
------------

Install from source
+++++++++++++++++++

.. code-block:: bash

   # clone repo
   git clone git@gitlab.com:Akm0d/idem_contracts.git
   cd idem_contracts

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install .

Usage
=====

Examples
--------

Run the given state to print the order of contract execution

.. code-block:: bash

   idem state state.sls

Output:

.. code-block::

   pre_pre_present
   pre_present
   post_pre_present
   pre_post_present
   post_post_present
